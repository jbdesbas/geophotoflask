from flask import render_template,request, flash,url_for,redirect,Response, session,abort,Blueprint, current_app,send_file,redirect,send_from_directory
from app.models import photo
from app.models.photo import db
import os
from datetime import datetime as dt
main = Blueprint('main', __name__, url_prefix='/',static_folder='static')


@main.route('/')
def index():
    if request.args.get('proj'):
        return redirect(url_for('.index_repo',repo=request.args.get('proj')))
    else:
        abort(403)

@main.route('/<repo>/')
@main.route('/<repo>')
def index_repo(repo):
    return render_template('index.html',repo=repo)
    
@main.route('/<repo>/scan')
def scan_repo(repo):
    t=dt.now()
    photo.scanDir(os.path.join(repo,'**'),thumbnail=True)
    db.clear_cache()
    current_app.logger.info('scan repo "{}" in {}'.format(repo,str(dt.now()-t)))
    return "ok"
    
@main.route('/view_img/<path:img>')
def view_img(img):
    size=request.args.get('size', None)
    return render_template('view_img.htm',img=img,size=size)
    
@main.route('/img/<size>/<path:img>')
def direct_img(img,size):
    if size=='original':
        return send_from_directory('static/phototheque/',filename=img,as_attachment=True)
    size=tuple(map(int,size.split('x')))
    thumb_name=photo.makeThumbnails(img,size,force=False)
    return send_from_directory('static/phototheque/resized/',thumb_name)


@main.route('/geojson/<repo>')
def geoJson(repo):
    return Response(photo.geoJson(repo),mimetype='application/json')
    
@main.route('/clear')
def clear_cache():
    current_app.logger.info('cleared cache')
    db.clear_cache();
    return "ok"
