mapboxgl.accessToken=$MAPBOX_TOKEN;
var map = new mapboxgl.Map({
    container: 'map',
    center: [2.268329,49.846264],
    //maxBounds:new mapboxgl.LngLatBounds(sw, ne),
    style: 'mapbox://styles/mapbox/streets-v9',
    zoom:12,
    pitch:0
});



map.on('load', function () {

    $.getJSON($SCRIPT_ROOT+'/geojson/'+$REPO,function(data){
        bbox=turf.bbox(data);
        map.fitBounds(bbox);
        
        map.addSource('photo', {
        type: 'geojson',
        data:data});
        
        map.addLayer({"id":"photo",
        "type": "circle",
        "source":"photo",
        "paint":
        {
            "circle-color":'#FF0000'
        }
        })
    

    });
    
    
    map.on('click', 'photo', function (e) {
        $("#myModal").modal('show');
        console.log(e.features);
        $.get($SCRIPT_ROOT+'img/'+e.features[0].properties.filename,function(data){$('.modal-body').html(data)});
    })

});