var map = L.map('map_leaflet').setView([49.846264,2.268329], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var photo_layer;
var clusterMarkers = L.markerClusterGroup({
    maxClusterRadius:70,
    iconCreateFunction:function(cluster){
        
        return new L.DivIcon({
            iconSize: [50, 50],
            html: '<div style="background-image: url(/img/100x100/' + cluster.getAllChildMarkers()[0].feature.properties.filename + ');"></div>​<b>' + cluster.getChildCount() + '</b>',
            className:'leaflet-marker-photo'})
        //return new L.Icon({iconUrl: cluster.getAllChildMarkers()[0].feature.properties.thumbnail,className:'leaflet-marker-photo-red'})
    }
    
});

$.getJSON($SCRIPT_ROOT+'/geojson/'+$REPO, function(data){
    photo_layer=L.geoJSON(data,
        {
            pointToLayer : function(f,latlng){
                var icon = new L.DivIcon({
                    iconSize: [50, 50],
                    html: '<div style="background-image: url(/img/100x100/' + f.properties.filename + '); background-size:cover;"></div>',
                    className:'leaflet-marker-photo'})
                //var icon = new L.Icon({iconUrl: f.properties.thumbnail,className:'leaflet-marker-photo'})
                return L.marker(latlng,{icon:icon})
                
            }
        }
        
    
    
    );
    
    clusterMarkers.addLayer(photo_layer);
	map.addLayer(clusterMarkers);
    clusterMarkers.bringToFront();
    
    map.fitBounds(photo_layer.getBounds());
    photo_layer.on('click',function(e){
        $('.modal-body').html('chargement..');
        $("#myModal").modal('show');
        $.get($SCRIPT_ROOT+'view_img/'+e.layer.feature.properties.filename,{size:'800x800'},function(data){$('.modal-body').html(data)});
    });
    
    
    
})