from flask import Flask
from flask_bootstrap import Bootstrap
import os
import logging, logging.handlers


bootstrap = Bootstrap()

def create_app():
    app = Flask(__name__)
    app.secret_key = os.environ.get('SECRET_KEY','testingSecretKey')
    
    fh = logging.handlers.TimedRotatingFileHandler(os.environ.get('LOGFILE','log/geophoto.log'),'D',30)
    fh.setFormatter(logging.Formatter('[%(asctime)s] %(levelname)s in %(module)s ( %(funcName)s ): %(message)s'))
    app.logger.addHandler(fh)
    app.logger.setLevel(logging.INFO)
    
    app.config['RESIZE_URL']=os.environ.get('RESIZE_URL')
    app.config['RESIZE_ROOT']=os.environ.get('RESIZE_ROOT')
    
    bootstrap.init_app(app)
    
    app.config['MAPBOX_TOKEN']=os.environ.get('MAPBOX_TOKEN')
    
    from app.views import main
    app.register_blueprint(main)
    return app