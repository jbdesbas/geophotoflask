import piexif, glob,json
from os import path
from math import floor
from PIL import Image
from tinydb import TinyDB, Query
import base64


photodir='/home/ql/tools/geophotoflask/app/static/phototheque/'


db=TinyDB('db.json')

def frac2dec(frac):
    return frac[0][0]/frac[0][1]+(frac[1][0]/60/frac[0][1])+(frac[2][0]/3600/frac[2][1])

def dec2frac(dec):
    m=(dec-floor(dec))*60
    s=(m-floor(m))*60
    return ((floor(dec),1),(floor(m),1),(floor(s*10000),10000))

def updateCoordinate(file,lat,lon,update_database=True): #TODO modify file and update db
    newfile=path.join(path.dirname(file),('_'+path.basename(file)))
    img = Image.open(file)
    info = img.info.get('exif',False)
    coord = { piexif.GPSIFD.GPSLatitude : dec2frac(lat) , piexif.GPSIFD.GPSLongitude : dec2frac(lon)}
    exif_dict.update({'GPS':coord})
    exif_bytes = piexif.dump(exif_dict)
    img.save(newfile, "jpeg", exif=exif_bytes)
    if update_database :
        updateDB(DBfile,lat,lon)

    
def updateDB(file,lat,lon): #Add or update a photo in the db
    img = Image.open(file)
    info = img.info.get('exif',False)
    relFile=path.relpath(file,photodir)
    db.remove(Query().filename == relFile)
    db.insert({'filename':relFile,'lon':lon,'lat':lat})


def scanFile(file):
    img = Image.open(file)
    info = img.info.get('exif',False)
    lat,lon=None,None
    if info:
        exif_dict = piexif.load(info)
        try :
            lat=frac2dec(exif_dict.get('GPS',{}).get(piexif.GPSIFD.GPSLatitude))
            lon=frac2dec(exif_dict.get('GPS',{}).get(piexif.GPSIFD.GPSLongitude))
            if exif_dict.get('GPS',{}).get(piexif.GPSIFD.GPSLongitudeRef,b'E').decode() == 'W':
                lon=-lon
        except TypeError:
            pass
    updateDB(file,lat,lon)

def scanDir(dir='test/**',thumbsize=('50x50','400x400'),thumbnail=False): #use ** for recursive
    print(path.join(photodir,dir))
    for p in glob.iglob(path.join(photodir,dir),recursive=True):
        if not (p.lower().endswith('.jpg') or p.lower().endswith('.jpeg')):
            continue
        if path.dirname(p).endswith('mini') or '_sfpg_data' in p.split('/') or 'resized-images' in p.split('/') or 'resized' in p.split('/'):
            continue
        scanFile(p)
        if thumbnail:
            makeThumbnails(path.relpath(p,photodir),(100,100))

def isValidCoordinates(lat,lon):
    if not lon or not lat :
        return False
    if lat<=-90 or lat>=90 or lon<=-180 or lon>=180 :
        return False
    return True

def geoJson(repo=None):
    out={'type':'FeatureCollection','features':[]}
    if repo :
        data=db.search(Query().filename.matches(r'.*{}/.*'.format(repo)))
    else :
        data=db.all()
    for e in data:
        if not isValidCoordinates(e.get('lat'),e.get('lon')):
            continue
        try :
            feature={}
            feature['type']='Feature'
            feature['properties']={'filename':e.get('filename')}
            feature['geometry']={'type':'Point','coordinates':[e.get('lon'),e.get('lat')]}
            out['features'].append(feature)
        except :
            continue
    return json.dumps(out)

def cleanDb(): #clean deleted file of db
    file_not_exist = lambda f : not path.exists(path.join(photodir,f))
    db.remove(Query().filename.test(file_not_exist))
        

def makeThumbnails(f,size,force=False,rotate=True):
    """
    Creer une miniature. La taille et le fichier d'origine sont encodées dans le nom du nouveau fichier
    :param f: Le chemin de l'image
    :param size: La taille de la miniature en px
    :param force: Recreer la miniature, même si elle existe déja
    :type f: str
    :type size: tuple
    :type force: boolean
    :return: Le nom de le miniature créée
    :rtype: str
    """
    #s = JSONWebSignatureSerializer('secret-key')
    par={'file':f,'size':size}
    #newname=s.dumps(par).decode()+'.jpg'
    newname=base64.urlsafe_b64encode(json.dumps(par).encode()).decode()+'.jpg'
    #print(f)
    if path.exists(path.join(photodir,'resized',newname)) and not force:
        print('exists')
        return newname
    im=Image.open(path.join(photodir,f))
    if rotate:
        im=autorotate(im)
    im.thumbnail(size)
    im.save(path.join(photodir,'resized',newname))
    return newname

def autorotate(f):
    """
    non testé, f est une image ou le chemin vers l'image
    """
    if isinstance(f,str):
        img = Image.open(f)
    else:
        img=f
    info = img.info.get('exif',False)
    exif_dict = piexif.load(info)
    orientation = exif_dict["0th"].get(piexif.ImageIFD.Orientation)
    if orientation == 2:
        img = img.transpose(Image.FLIP_LEFT_RIGHT)
    elif orientation == 3:
        img = img.rotate(180)
    elif orientation == 4:
        img = img.rotate(180).transpose(Image.FLIP_LEFT_RIGHT)
    elif orientation == 5:
        img = img.rotate(-90, expand=True).transpose(Image.FLIP_LEFT_RIGHT)
    elif orientation == 6:
        img = img.rotate(-90, expand=True)
    elif orientation == 7:
        img = img.rotate(90, expand=True).transpose(Image.FLIP_LEFT_RIGHT)
    elif orientation == 8:
        img = img.rotate(90, expand=True)
    return img


#autorotate : https://gist.github.com/dangtrinhnt/a577ece4cbe5364aad28

#https://stackoverflow.com/questions/30469575/how-to-pickle-and-unpickle-to-portable-string-in-python-3